package com.example.proyectoandroidv2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.auth.User;

import java.util.List;

public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.ViewHolder> {

    private Context context;

    public List<Users> usersList;
    public UsersListAdapter(List<Users> usersList){
        this.usersList=usersList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nameText.setText(usersList.get(position).getNombre());
        holder.rolText.setText(usersList.get(position).getRol());
        holder.emailText.setText(usersList.get(position).getEmail());

    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public TextView nameText,rolText,emailText;
        private Button btnEdit,btndelete;


        public  ViewHolder(View itemView){
            super(itemView);
            mView=itemView;
            nameText=(TextView) mView.findViewById(R.id.name_text);
            rolText=(TextView) mView.findViewById(R.id.rol_text);
            emailText=(TextView) mView.findViewById(R.id.email_text);
            btnEdit= mView.findViewById(R.id.add_btn);
            //btndelete=mView.findViewById(R.id.delete_btn);
            notifyDataSetChanged();
        }

    }
    public void edit(View view){
        //User user= usersList.get();
        //Intent intent= new Intent(context,ChangePassword.class);
       // Users user = getItem(getPosition());
    }





}
