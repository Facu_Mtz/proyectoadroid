package com.example.proyectoandroidv2;

import java.io.Serializable;

public class reports implements Serializable {
    String service;
    String date;
    String idEquip;
    String nameTec;
    String client;
    String ram;
    String procesador;
    String marca;
    String rol;


    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;

    public reports(){

    }
    public reports(String service, String date,String client,String ram,String procesador,String marca){
        this.date=date;
        this.service=service;

        this.client=client;
        this.marca=marca;
        this.ram=ram;
        this.procesador=procesador;
    }
    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdEquip() {
        return idEquip;
    }

    public void setIdEquip(String idEquip) {
        this.idEquip = idEquip;
    }

    public String getNameTec() {
        return nameTec;
    }

    public void setNameTec(String nameTec) {
        this.nameTec = nameTec;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }
}
