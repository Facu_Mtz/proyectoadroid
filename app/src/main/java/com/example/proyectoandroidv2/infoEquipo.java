package com.example.proyectoandroidv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class infoEquipo extends AppCompatActivity {

    FirebaseAuth fauth;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    String sessionEm;
    String currentrol;
    TextView tmarca,tproce,tram;
    TextView ser,dat,clie,nomTec;
    private reports report;
    Button back;




    Uri FilePathUri;
    Uri File;
    StorageReference storageReference;
    DatabaseReference databaseReference;
    //Map<String, Object> report = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_equipo2);

        tproce=findViewById(R.id.textproce);
        tmarca=findViewById(R.id.textmarca);
        tram=findViewById(R.id.textram);


        fauth = FirebaseAuth.getInstance();
        sessionEm=getIntent().getStringExtra("currentEmail");
        storageReference = FirebaseStorage.getInstance().getReference("qr");
        databaseReference = FirebaseDatabase.getInstance().getReference("qr");
        //sessionEm=getIntent().getStringExtra("currentEmail");

        //report= (reports) getIntent().getSerializableExtra("report");
        String idequip=getIntent().getStringExtra("idequip");

        //currentrol=getIntent().getStringExtra("currentRol");
        sessionEm=getIntent().getStringExtra("currentEmail");

        db= FirebaseFirestore.getInstance();

        DocumentReference docRef=db.collection("reporte").document(idequip);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot document=task.getResult();
                    if(document.exists()){
                        reports report = document.toObject(reports.class);
                        tproce.setText(report.getProcesador());
                        tmarca.setText(report.getMarca());
                        tram.setText(report.getRam());
                        Log.d("test","Document: " +document.getData());

                    }else{
                        Log.d("test","No encontro el documento");
                    }
                }else{
                    Log.d("test","Ocurrio un error: " +task.getException());
                }
            }
        });

        /*Log.i("Test", report.getClient());
        Log.i("Test", report.getDate());
        Log.i("Test", report.getNameTec());
        Log.i("Test", report.getService());*/



/*
        tproce.setText(report.getProcesador());
        tmarca.setText(report.getMarca());
        tram.setText(report.getRam());*/
        back=findViewById(R.id.buttonBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),qrOption.class).putExtra("idequip", idequip).putExtra("currentEmail",sessionEm));
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        final MenuItem listUsersss= menu.findItem(R.id.drawListUser);
        final MenuItem addUser= menu.findItem(R.id.drawAddReport);
        //final MenuItem reportS= menu.findItem(R.id.drawReportSign);
        final MenuItem rCamera= menu.findItem(R.id.icCamera);
        //final MenuItem rQR= menu.findItem(R.id.addQR);

        db.collection("usuarios").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for(QueryDocumentSnapshot document : task.getResult()) {
                    Users user = document.toObject(Users.class);

                    //mUserList.clear();
                    //mUserList.add(user);
                    user.setId(document.getId());

                    Log.d("test", sessionEm, task.getException());
                    if(user.getEmail().equals(sessionEm) ){
                        currentrol=user.getRol();
                        Log.d("test", currentrol, task.getException());
                        if(currentrol.equals("Admin")){

                        }else{
                            listUsersss.setVisible(false);
                            if (currentrol.equals("Tecnico")) {


                            }else {
                                addUser.setVisible(false);
                                //reportS.setVisible(false);
                                rCamera.setVisible(false);
                                //rQR.setVisible(false);
                            }
                        }
                    }
                }


            }
        });
        //MenuItem list2=menu.findItem(R.id.draLogout);
        /*if(currentrol != "Admin") {
            listUsersss.setVisible(false);
        } *//*else*/


        //String temps=currentrol.toString();


        /*if(currentrol != "Admin") {
            listUsersss.setVisible(false);
            if (currentrol != "Tecnico") {
                addUser.setVisible(false);
                reportS.setVisible(false);
                rCamera.setVisible(false);
                rQR.setVisible(false);

            }else {

            }
        }else{
            addUser.setVisible(true);
            reportS.setVisible(true);
            rCamera.setVisible(true);
            rQR.setVisible(true);
        }*/

        //list2.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.drawListUser:
                startActivity(new Intent(getApplicationContext(),MainActivity.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawAddReport:
                startActivity(new Intent(getApplicationContext(),reporte.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawListReport:
                startActivity(new Intent(getApplicationContext(),listaReportes.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.draLogout:
                FirebaseAuth.getInstance().signOut();//logout
                startActivity(new Intent(getApplicationContext(),Login.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.icCamera:
                startActivity(new Intent(getApplicationContext(),activityQR.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            /*case R.id.drawReportSign:
                startActivity(new Intent(getApplicationContext(),reportSign.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.addQR:
                startActivity(new Intent(getApplicationContext(),createQr.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }
}
