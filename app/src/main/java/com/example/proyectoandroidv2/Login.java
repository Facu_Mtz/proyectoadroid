package com.example.proyectoandroidv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class Login extends AppCompatActivity {

    Button mloginButton;
    EditText mpassword,memail;
    FirebaseAuth fauth;
    ProgressBar progressBar2;
    FirebaseFirestore db=FirebaseFirestore.getInstance();
    private String email,password;
    String temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        memail    =   findViewById(R.id.logemail);
        mpassword    =   findViewById(R.id.logpassword);
        mloginButton = findViewById(R.id.loginuser);
        progressBar2 = findViewById(R.id.progressBar2);
        fauth=FirebaseAuth.getInstance();




        mloginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = memail.getText().toString().trim();
                password= mpassword.getText().toString().trim();
                if(TextUtils.isEmpty(email)){
                    memail.setError("Se requiere un email");
                    return;
                }
                if (TextUtils.isEmpty(password)){
                    mpassword.setError("Se requiere un password");
                    return;
                }
                if(password.length() < 6){
                    mpassword.setError("El Password debe tener mas de 6 caracteres");
                    return;
                }
                progressBar2.setVisibility(View.VISIBLE);

                //authenticate
                db.collection("usuarios").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        List<Users> mUserList = new ArrayList<>();
                        if(task.isSuccessful()){
                            for(QueryDocumentSnapshot document : task.getResult()) {
                                Users user = document.toObject(Users.class);

                                //mUserList.clear();
                                mUserList.add(user);
                                user.setId(document.getId());
                                String emails=email.toLowerCase();

                                if(email.equalsIgnoreCase(user.getEmail())){

                                    fauth.signInWithEmailAndPassword(emails,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if(task.isSuccessful()){
                                                temp=user.getRol();
                                                if(temp.equalsIgnoreCase("cliente")){
                                                    Log.d("test", temp);
                                                    Toast.makeText(Login.this, "Login con exito", Toast.LENGTH_SHORT).show();
                                                    //startActivity(new Intent(getApplicationContext(),MainActivity.class));
                                                    Intent i = new Intent(getApplicationContext(), listaReportes.class);
                                                    //startActivity(i);
                                                    i.putExtra("currentEmail", email);
                                                    startActivity(i);
                                                }else {
                                                    Log.d("test", temp);
                                                    Toast.makeText(Login.this, "Login con exito", Toast.LENGTH_SHORT).show();
                                                    //startActivity(new Intent(getApplicationContext(),MainActivity.class));
                                                    Intent i = new Intent(getApplicationContext(), reporte.class);
                                                    //startActivity(i);
                                                    i.putExtra("currentEmail", email);
                                                    startActivity(i);
                                                }
                                            }else{
                                                //Toast.makeText(Login.this,"Error: "+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                                                Toast.makeText(Login.this,"Correo o password incorrecto",Toast.LENGTH_SHORT).show();
                                                progressBar2.setVisibility(View.GONE);
                                            }
                                        }
                                    });
                                }else{
                                    //Toast.makeText(Login.this,"No coincide con ningun email",Toast.LENGTH_SHORT).show();
                                }
                            }


                        } else {
                            Log.d("MissionActivity", "Error getting documents: ", task.getException());
                        }
                    }
                });

            }

        });
    }
}
