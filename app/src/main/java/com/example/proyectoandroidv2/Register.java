package com.example.proyectoandroidv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {

    Button registerButton,login;
    EditText mname,mrole,mpassword,memail;
    FirebaseAuth fauth;
    ProgressBar progressBar;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    RadioButton rAdm,rCli,rTec;
    String currentrol;
    String sessionEm;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mname    =   findViewById(R.id.name_text);
        //mrole    =   findViewById(R.id.rol_text);
        memail    =   findViewById(R.id.email_text);
        mpassword    =   findViewById(R.id.password);
        registerButton = findViewById(R.id.registerUser);
        login=findViewById(R.id.goToMain);
        rAdm   =   findViewById(R.id.radioAdmin);
        rCli    =   findViewById(R.id.radioCliente);
        rTec    =   findViewById(R.id.radioTecnico);


        fauth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressBar);


        //currentrol=getIntent().getStringExtra("currentRol");
        sessionEm=getIntent().getStringExtra("currentEmail");



        /*if(fauth.getCurrentUser() !=null){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
            finish();
        }*/
        /*private void writeNewUser(String userId,String name,String email){
            User user=new User(name,email);

        }*/


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class).putExtra("currentEmail",sessionEm));
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                String email = memail.getText().toString().toLowerCase().trim();
                Log.d("test",email);
                String password= mpassword.getText().toString().trim();
                String service="";
                if(TextUtils.isEmpty(email)){
                    memail.setError("Se requiere un email");
                    return;
                }
                if (TextUtils.isEmpty(password)){
                    mpassword.setError("Se requiere un password");
                    return;
                }
                if(password.length() < 6){
                    mpassword.setError("El Password debe tener mas de 6 caracteres");
                    return;
                }
                if(rAdm.isChecked()==true){
                    service="Admin";
                    Toast.makeText(Register.this,service,Toast.LENGTH_SHORT).show();


                }
                if(rCli.isChecked()==true){
                    service="Cliente";
                    Toast.makeText(Register.this,service,Toast.LENGTH_SHORT).show();

                }
                if(rTec.isChecked()==true){
                    service="Tecnico";
                    Toast.makeText(Register.this,service,Toast.LENGTH_SHORT).show();

                }
                progressBar.setVisibility(View.VISIBLE);

                //registrar el usuario en firebase
                final String finalService = service;
                fauth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>(){

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            String email2 = memail.getText().toString().trim();
                            String name2= mname.getText().toString().trim();
                            //String rol2= mrole.getText().toString().trim();
                            Map<String,Object> user=new HashMap<>();
                            user.put("nombre",name2);
                            user.put("email",email2);
                            user.put("rol", finalService);
                            db.collection("usuarios").add(user)
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            Log.d("TAG","Se agegaron los datos con el id: "+documentReference.getId());

                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w("TAG","Error al agregar los datos",e);
                                }
                            });
                            Toast.makeText(Register.this,"Se a creado el usuario.",Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),MainActivity.class).putExtra("currentEmail",sessionEm));
                            /*userID=fauth.getCurrentUser().getUid();
                            DocumentReference doc*/
                        }else{
                            Toast.makeText(Register.this,"Error: "+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);

                        }
                    }
                });
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        final MenuItem listUsersss= menu.findItem(R.id.drawListUser);
        final MenuItem addUser= menu.findItem(R.id.drawAddReport);
        //final MenuItem reportS= menu.findItem(R.id.drawReportSign);
        final MenuItem rCamera= menu.findItem(R.id.icCamera);
        //final MenuItem rQR= menu.findItem(R.id.addQR);

        db.collection("usuarios").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for(QueryDocumentSnapshot document : task.getResult()) {
                    Users user = document.toObject(Users.class);

                    //mUserList.clear();
                    //mUserList.add(user);
                    user.setId(document.getId());

                    Log.d("test", sessionEm, task.getException());
                    if(user.getEmail().equals(sessionEm) ){
                        currentrol=user.getRol();
                        Log.d("test", currentrol, task.getException());
                        if(currentrol.equals("Admin")){

                        }else{
                            listUsersss.setVisible(false);
                            if (currentrol.equals("Tecnico")) {


                            }else {
                                addUser.setVisible(false);
                                //reportS.setVisible(false);
                                rCamera.setVisible(false);
                                //rQR.setVisible(false);
                            }
                        }
                    }
                }


            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.drawListUser:
                startActivity(new Intent(getApplicationContext(),MainActivity.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawAddReport:
                startActivity(new Intent(getApplicationContext(),reporte.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawListReport:
                startActivity(new Intent(getApplicationContext(),listaReportes.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.draLogout:
                FirebaseAuth.getInstance().signOut();//logout
                startActivity(new Intent(getApplicationContext(),Login.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.icCamera:
                startActivity(new Intent(getApplicationContext(),activityQR.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            /*case R.id.drawReportSign:
                startActivity(new Intent(getApplicationContext(),reportSign.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.addQR:
                startActivity(new Intent(getApplicationContext(),createQr.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }
}
