package com.example.proyectoandroidv2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class CustomAdapterReports extends ArrayAdapter<reports> {
    FirebaseFirestore db=FirebaseFirestore.getInstance();
    private List<reports> listReport;
    //private List<Tasks> dataitems;
    private Context context;
    private View v;
    String sessionEm;
    private Activity activity;




    public CustomAdapterReports(Activity activity, List<reports> listReport) {
        super(activity, 0, listReport);
        this.activity=activity;
        this.listReport=listReport;

    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.list_reports, parent, false);
        }
        TextView serviceText = (TextView) convertView.findViewById(R.id.textService);
        TextView dateText = (TextView) convertView.findViewById(R.id.textDate);
        TextView clientText = (TextView) convertView.findViewById(R.id.textClient);
        Button btnDetails= convertView.findViewById(R.id.btnDetail);
        reports report = getItem(position);
        sessionEm=report.getRol();
        Log.d("test","email "+ sessionEm);

        clientText.setText(report.getClient());
        dateText.setText(report.getDate());
        serviceText.setText(report.getService());

        //Boton para poder ver detalles del servicio
        btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                reports report= getItem(position);
                Intent i = new Intent(activity,Details.class);
                i.putExtra("report",report);
                i.putExtra("currentEmail",sessionEm);
                activity.startActivity(i);

            }
        });



        return convertView;
    }

}
