package com.example.proyectoandroidv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class MainActivity extends AppCompatActivity {
    //private Toolbar toolbar;
    private ListView listView;
    FirebaseAuth fauth;
    private ArrayList<String> arrayList=new ArrayList<>();
    private Adapter adapter;
    private DatabaseReference mDatabase;
    FirebaseFirestore db=FirebaseFirestore.getInstance();
    private RecyclerView mMainList;
    private List<Users> usersList;
    private UsersListAdapter usersListAdapter;
    private String s;
    private  Context mchart;
    Button btnedit,btndelte;
    private FirebaseFirestore test;
    private RecyclerView recyclerView;
    String sessionEm;
    String currentrol;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*toolbar=findViewById(R.id.myToolbar);

        setSupportActionBar(toolbar);*/


        fauth = FirebaseAuth.getInstance();
        //sessionEm=getIntent().getStringExtra("currentEmail");
        //adapter= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayList);


        //usersList=new ArrayList<>();
       // usersListAdapter=new UsersListAdapter(usersList);

        //adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        //adapter= new CustomAdapter(this,usersList);
        //recyclerView.setAdapter(adapter);



        /*mMainList=(RecyclerView) findViewById(R.id.data_list);
        mMainList.setHasFixedSize(true);
        mMainList.setLayoutManager(new LinearLayoutManager(this));
        mMainList.setAdapter(usersListAdapter);*/

        //currentrol=getIntent().getStringExtra("currentRol");
        sessionEm=getIntent().getStringExtra("currentEmail");



        db.collection("usuarios").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                List<Users> mUserList = new ArrayList<>();
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()) {
                        Users user = document.toObject(Users.class);

                        //mUserList.clear();
                        mUserList.add(user);
                        user.setId(document.getId());

                    }
                    //adapter.notifyDataSetChanged();
                    ListView mUserListView = (ListView) findViewById(R.id.listview);


                    CustomAdapter mUserAdapter = new CustomAdapter(MainActivity.this, mUserList);
                    mUserListView.setAdapter(mUserAdapter);
                    //adapter.clear();


                    //mUserListView.deferNotifyDataSetChanged();
                } else {
                    Log.d("MissionActivity", "Error getting documents: ", task.getException());
                }
            }
        });

        //FirestoresRecyclerOptions<Users>


        //Borrar un documento firestore
        //db.collection("usuarios").

        //listView=(ListView) findViewById(R.id.data_list);
        /*db.collection("usuarios").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot documentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(e !=null){
                    Log.d("TAG","Error: "+e.getMessage());
                }
                /*for(DocumentSnapshot doc : documentSnapshots){
                    String user_name = doc.getString("nombre");
                    Log.d("TAG","Name: "+user_name);
                }*/
               /* for(DocumentChange doc : documentSnapshots.getDocumentChanges()){
                    if(doc.getType() == DocumentChange.Type.ADDED){
                        /*String user_name = doc.getDocument().getString("nombre");
                        Log.d("TAG","Name: "+user_name);*/
                     /*   Users users=doc.getDocument().toObject(Users.class);
                        usersList.add(users);
                        usersListAdapter.notifyDataSetChanged();
                    }

                }
            }
        });*/

    }

    public void refresh(Context context){
        listView.invalidate();
    }

    public void logout1(View view) {
        FirebaseAuth.getInstance().signOut();//logout
        startActivity(new Intent(getApplicationContext(),Login.class));
        finish();
    }

    public void createUser(View view) {
        startActivity(new Intent(getApplicationContext(),Register.class).putExtra("currentEmail",sessionEm));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        final MenuItem listUsersss= menu.findItem(R.id.drawListUser);
        final MenuItem addUser= menu.findItem(R.id.drawAddReport);
        //final MenuItem reportS= menu.findItem(R.id.drawReportSign);
        final MenuItem rCamera= menu.findItem(R.id.icCamera);
        //final MenuItem rQR= menu.findItem(R.id.addQR);

        db.collection("usuarios").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for(QueryDocumentSnapshot document : task.getResult()) {
                    Users user = document.toObject(Users.class);

                    //mUserList.clear();
                    //mUserList.add(user);
                    user.setId(document.getId());

                    Log.d("test", sessionEm, task.getException());
                    if(user.getEmail().equals(sessionEm) ){
                        currentrol=user.getRol();
                        Log.d("test", currentrol, task.getException());
                        if(currentrol.equals("Admin")){

                        }else{
                            listUsersss.setVisible(false);
                            if (currentrol.equals("Tecnico")) {


                            }else {
                                addUser.setVisible(false);
                                //reportS.setVisible(false);
                                rCamera.setVisible(false);
                                //rQR.setVisible(false);
                            }
                        }
                    }
                }


            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.drawListUser:
                startActivity(new Intent(getApplicationContext(),MainActivity.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawAddReport:
                startActivity(new Intent(getApplicationContext(),reporte.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawListReport:
                startActivity(new Intent(getApplicationContext(),listaReportes.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.draLogout:
                FirebaseAuth.getInstance().signOut();//logout
                startActivity(new Intent(getApplicationContext(),Login.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.icCamera:
                startActivity(new Intent(getApplicationContext(),activityQR.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            /*case R.id.drawReportSign:
                startActivity(new Intent(getApplicationContext(),reportSign.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.addQR:
                startActivity(new Intent(getApplicationContext(),createQr.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }
}
