package com.example.proyectoandroidv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class ChangePassword extends AppCompatActivity {

    EditText editname,editrol;
    FirebaseAuth auth;
    private Users user;
    private FirebaseFirestore db;
    Button updateRol,updatepass,btndelete;
    String sessionEm;
    String currentrol;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sessionEm=getIntent().getStringExtra("currentEmail");
        user= (Users) getIntent().getSerializableExtra("user");
        db=FirebaseFirestore.getInstance();

        setContentView(R.layout.activity_change_password);
        editname=findViewById(R.id.editname);
        editrol=findViewById(R.id.editRol);
        auth = FirebaseAuth.getInstance();
        updateRol=findViewById(R.id.updateUser);
        btndelete=findViewById(R.id.btndelete);


        //sessionEm=getIntent().getStringExtra("currentEmail");

        editrol.setText(user.getRol());
        editname.setText(user.getNombre());

        updateRol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editUser();
            }
        });


        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteUser();
            }
        });

    }

    public void changePassword(View v){
        //FirebaseUser user=FirebaseAuth.getInstance().get
    }

    public void editUser(){
        String editr=editrol.getText().toString().trim();
        String email=user.getEmail().toString().trim();
        String nombre=editname.getText().toString().trim();
        Map<String,Object> user2=new HashMap<>();
        user2.put("nombre",nombre);
        user2.put("email",email);
        user2.put("rol",editr);

        db.collection("usuarios").document(user.getId()).set(user2).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                Toast.makeText(ChangePassword.this,"Se acutalizo el usuario",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),MainActivity.class).putExtra("currentEmail",sessionEm));
            }
        });
    }
    public void deleteUser(){
        db.collection("usuarios").document(user.getId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(ChangePassword.this,"Se a borrado el usuario",Toast.LENGTH_SHORT).show();
                    finish();
                    startActivity(new Intent(ChangePassword.this,MainActivity.class).putExtra("currentEmail",sessionEm));
                }
            }
        });
    }

    public void onClick(View v){

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        final MenuItem listUsersss= menu.findItem(R.id.drawListUser);
        final MenuItem addUser= menu.findItem(R.id.drawAddReport);
        //final MenuItem reportS= menu.findItem(R.id.drawReportSign);
        final MenuItem rCamera= menu.findItem(R.id.icCamera);
        //final MenuItem rQR= menu.findItem(R.id.addQR);

        db.collection("usuarios").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for(QueryDocumentSnapshot document : task.getResult()) {
                    Users user = document.toObject(Users.class);

                    //mUserList.clear();
                    //mUserList.add(user);
                    user.setId(document.getId());

                    Log.d("test", sessionEm, task.getException());
                    if(user.getEmail().equals(sessionEm) ){
                        currentrol=user.getRol();
                        Log.d("test", currentrol, task.getException());
                        if(currentrol.equals("Admin")){

                        }else{
                            listUsersss.setVisible(false);
                            if (currentrol.equals("Tecnico")) {


                            }else {
                                addUser.setVisible(false);
                                //reportS.setVisible(false);
                                rCamera.setVisible(false);
                                //rQR.setVisible(false);
                            }
                        }
                    }
                }


            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.drawListUser:
                startActivity(new Intent(getApplicationContext(),MainActivity.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawAddReport:
                startActivity(new Intent(getApplicationContext(),reporte.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawListReport:
                startActivity(new Intent(getApplicationContext(),listaReportes.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.draLogout:
                FirebaseAuth.getInstance().signOut();//logout
                startActivity(new Intent(getApplicationContext(),Login.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.icCamera:
                startActivity(new Intent(getApplicationContext(),activityQR.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            /*case R.id.drawReportSign:
                startActivity(new Intent(getApplicationContext(),reportSign.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.addQR:
                startActivity(new Intent(getApplicationContext(),createQr.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }



}
