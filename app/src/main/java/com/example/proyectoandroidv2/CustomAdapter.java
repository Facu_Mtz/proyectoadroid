package com.example.proyectoandroidv2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;

import java.util.ArrayList;
import java.util.List;

import static androidx.core.content.ContextCompat.startActivity;

public class CustomAdapter extends ArrayAdapter<Users> {
    FirebaseFirestore db=FirebaseFirestore.getInstance();
    private List<Users> listUser;
    //private List<Tasks> dataitems;
    private  Context context;
    private View v;
    private Activity activity;
    String sessionEm;




    public CustomAdapter(Activity activity, List<Users> listUser) {
        super(activity, 0, listUser);
        this.activity=activity;
        this.listUser=listUser;

    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.list_item, parent, false);
        }
        //LayoutInflater inflater =LayoutInflater.from()
        TextView nameTextView = (TextView) convertView.findViewById(R.id.name_text);
        TextView rolTextView = (TextView) convertView.findViewById(R.id.rol_text);
        TextView emailTextView = (TextView) convertView.findViewById(R.id.email_text);
        Button btnEdit= convertView.findViewById(R.id.add_btn);
        //Button btnDelete= convertView.findViewById(R.id.delete_btn);
        Users user = getItem(position);


        nameTextView.setText(user.getNombre());
        rolTextView.setText(user.getRol().toString());
        emailTextView.setText(user.getEmail());

        //Boton para poder editar usuarios
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Users user= getItem(position);
               //Intent intent= new Intent(context,ChangePassword.class);
               //intent.putExtra("user",user);
               //context.startActivity(intent);
                //Users user = getItem(getPosition());
                //Toast.makeText(getContext(),"Position: "+user.getId(),Toast.LENGTH_SHORT).show();
                Intent i = new Intent(activity,ChangePassword.class);
                //startActivity(i);
                i.putExtra("user",user);
                Log.d("test", "pruebas");
                Log.d("test", user.getEmail());
                Log.d("test", user.getNombre());
                Log.d("test", user.getRol());
                sessionEm=user.getEmail();
                Log.d("test", sessionEm);
                i.putExtra("currentEmail",sessionEm);
                activity.startActivity(i);
                //v.getContext().startActivity(this,ChangePassword.class);
                //context.startActivity(this,ChangePassword.class);

               /*db.collection("usuarios").document(user.getId()).set().addOnSuccessListener(new OnSuccessListener<Void>() {
                   @Override
                   public void onSuccess(Void aVoid) {

                   }
               });*/
            }
        });
        //Boton para poder borrar usuarios
        /*btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Users user= getItem(position);
                db.collection("usuarios").document(user.getId()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                           // refreshAdapter((List<Users>) user);
                        notifyDataSetChanged();//no marca error pero no se actualiza
                        //Toast.makeText(context,user.getId(),Toast.LENGTH_SHORT).show();
                        //notifyDataSetChanged();
                        //Intent intent=new Intent(context,MainActivity.class);
                        //((MainActivity)context).refresh(context);
                        //context.startActivity(MainActivity.class);
                    }
                });
            }
        });*/


        return convertView;
    }

    public synchronized void refreshAdapter(List<Users> dataitems){
        dataitems.clear();
        dataitems.addAll(dataitems);
        notifyDataSetChanged();
    }


}
