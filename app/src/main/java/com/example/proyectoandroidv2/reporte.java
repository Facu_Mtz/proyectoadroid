package com.example.proyectoandroidv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class reporte extends AppCompatActivity {


    RadioButton rMant,rArreglo,rIngreso;
    String TAG="GenerateQRCode";
    Button btnReporte,btnCQR;
    EditText bNAme,bClient,bDate,idEquip;
    FirebaseAuth fauth;
    Createreport tempReport;
   // ProgressBar progressBar;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    String sessionEm;
    String currentrol;
    String temQr;

    private static final String IMAGE_DIRECTORY = "/qr";
    QRGEncoder qrgEncoder;
    String path;
    String namefile;
    String namepath;
    String urlImage;
    String inputvalue;
    Bitmap bitmap;
    ImageView qrimg;

    ProgressDialog progressDialog ;

    Uri FilePathUri;
    Uri File;
    StorageReference storageReference;
    DatabaseReference databaseReference;
    Map<String, Object> report = new HashMap<>();

    EditText bNameProce,bNameMarca,bnameRam;
    TextView tempIds,temNew;
    String client ,service,dates,tecnico,idEqui;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte);

        //pedir permiross
        ActivityCompat.requestPermissions(reporte.this,
                new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.INTERNET
                },
                100);
        bNAme    =   findViewById(R.id.addTecnico);
        bClient =   findViewById(R.id.addClient);
        bDate   =   findViewById(R.id.addDate);
        progressDialog = new ProgressDialog(reporte.this);



        fauth = FirebaseAuth.getInstance();
        sessionEm=getIntent().getStringExtra("currentEmail");
        storageReference = FirebaseStorage.getInstance().getReference("qr");
        databaseReference = FirebaseDatabase.getInstance().getReference("qr");

        bnameRam=findViewById(R.id.nameRam);
        bNameMarca=findViewById(R.id.nameMarca);
        bNameProce=findViewById(R.id.nameProce);
        btnCQR=findViewById(R.id.btnCreateQR);
        btnReporte=findViewById(R.id.btnRepo);
        qrimg=  findViewById(R.id.qrCodeIm);
        //tempIds=findViewById(R.id.tempId);

        /*fauth = FirebaseAuth.getInstance();
        sessionEm=getIntent().getStringExtra("currentEmail");
        storageReference = FirebaseStorage.getInstance().getReference("qr");
        databaseReference = FirebaseDatabase.getInstance().getReference("qr");
        sessionEm=getIntent().getStringExtra("currentEmail");*/

        /*client = getIntent().getStringExtra("client");
        dates= getIntent().getStringExtra("date");
        tecnico= getIntent().getStringExtra("nameTec");

        service=getIntent().getStringExtra("service");*/
        //progressDialog = new ProgressDialog(reporte.this);




        //report.put("idEquip", idEqui);
        btnReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                String marca= bNameMarca.getText().toString().trim();
                String procesador= bNameProce.getText().toString().trim();
                String ram= bnameRam.getText().toString().trim();
                String client = bClient.getText().toString().trim();
                String dates= bDate.getText().toString().trim();
                String tecnico= bNAme.getText().toString().trim();
               // String idEqui= idEquip.getText().toString().trim();
                report.put("client", client);
                report.put("date", dates);
                report.put("nameTec", tecnico);
                report.put("service", "Ingreso");
                report.put("procesador", marca);
                report.put("marca", procesador);
                report.put("ram", ram);
                //BitMatrix bitMatrix=multiFormatWriter.encode("Date: "+dates+"\n"+ " Cliente: " + client +"\n" + " Servicio: "+ service + "\n"+" Tecnico: "+tecnico, BarcodeFormat.QR_CODE,200,200);



                //if (inputvalue.length() > 0) {

                db.collection("reporte").add(report)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                /*client = getIntent().getStringExtra("client");
                                dates= getIntent().getStringExtra("date");
                                tecnico= getIntent().getStringExtra("nameTec");
                                idEqui=documentReference.getId();
                                Log.d("test", idEqui);

                                //tempReport.setIdEquip(idEqui);
                                tempIds.setText(idEqui);
                                service=getIntent().getStringExtra("service");*/
                                /*idEqui=documentReference.getId();
                                tempIds.setText(idEqui);*/
                                Log.d("TAG", "Se agegaron los datos con el id: " + documentReference.getId());
                                //temNew=findViewById(R.id.tempId);
                                Log.d("test", documentReference.getId());
                               // temQr = "Id Equipo: " + documentReference.getId();
                                temQr =documentReference.getId();
                                Log.d("test", temQr);
                                WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
                                Display display = manager.getDefaultDisplay();
                                Point point = new Point();
                                display.getSize(point);
                                int width = point.x;
                                int height = point.y;
                                int smallerdimension = width < height ? width : height;
                                smallerdimension = smallerdimension * 3 / 4;
                                qrgEncoder = new QRGEncoder(temQr, null, QRGContents.Type.TEXT, smallerdimension);
                                try {
                                    bitmap = qrgEncoder.encodeAsBitmap();
                                    qrimg.setImageBitmap(bitmap);
                                } catch (WriterException e) {
                                    Log.v(TAG, e.toString());
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "Error al agregar el reporte", e);
                    }
                });
                //tempIds=findViewById(R.id.tempId);
                btnCQR.setVisibility(View.VISIBLE);



            }
        });

        btnCQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BitmapDrawable drawable=(BitmapDrawable) qrimg.getDrawable();
                Bitmap bitmap=drawable.getBitmap();
                path = saveImage(bitmap);
                UploadImage();
            }
        });
    }


    public String saveImage(Bitmap myBitmap) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //Bitmap bitmap=drawable.getBitmap();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        java.io.File filepath= new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY /*iDyme folder*/);
        if (!filepath.exists()) {
            filepath.mkdirs();
            Log.d("test",filepath.toString());
        }
        try {
            File f = new File(filepath, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(reporte.this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            namefile=f.getName();
            namepath=f.getAbsolutePath();

            FilePathUri= Uri.fromFile(new File(namepath));
            File=Uri.fromFile(new File(namefile));
            Log.i("Test",f.getPath());
            Log.i("Test",f.getName());
            Log.i("Test",f.getAbsolutePath());
            Log.i("Test",f.toString());
            Log.i("Test",f.getParent());


            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public void UploadImage() {
        // FilePathUri=namepath.toString();
        if (FilePathUri != null) {

            progressDialog.setTitle("Image is Uploading...");
            progressDialog.show();
            urlImage = System.currentTimeMillis() + ".jpg";
            final StorageReference storageReference2 = storageReference.child(urlImage);
            storageReference2.putFile(FilePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            String TempImageName = namefile;
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Image Uploaded Successfully ", Toast.LENGTH_LONG).show();
                            @SuppressWarnings("VisibleForTests")
                            uploadinfo imageUploadInfo = new uploadinfo(TempImageName, taskSnapshot.getUploadSessionUri().toString());
                            String ImageUploadId = databaseReference.push().getKey();
                            databaseReference.child(ImageUploadId).setValue(imageUploadInfo);
                            Log.i("Test2", ImageUploadId);
                            Log.i("Test2", databaseReference.toString());
                            storageReference2.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Uri downloadUrl = uri;
                                    Log.i("Test2", downloadUrl.toString());
                                    //Do what you want with the url
                                    //String dates = edText.getText().toString().trim();
                                    //String service= edText2.getText().toString().trim();
                                    String urldef = downloadUrl.toString();
                                    //String cliente=edText3.getText().toString().trim();

                                    Map<String, Object> qrCol = new HashMap<>();

                                    qrCol.put("service", "QR Image");
                                    qrCol.put("url", urldef);

                                    /*String marca= bNameMarca.getText().toString().trim();
                                    String procesador= bNameProce.getText().toString().trim();
                                    String ram= bnameRam.getText().toString().trim();
                                    report.put("marca",marca);
                                    report.put("Procesador",procesador);
                                    report.put("Ram",ram);
                                    report.put("url",urldef);*/


                                    db.collection("qr").add(report)
                                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                                @Override
                                                public void onSuccess(DocumentReference documentReference) {
                                                    Log.d("TAG", "Se agegaron los datos con el id: " + documentReference.getId());

                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w("TAG", "Error al agregar la imagen", e);
                                        }
                                    });
                                    Toast.makeText(reporte.this, "Se a agregado la imagen qr", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

        } else {

            Toast.makeText(reporte.this, "Agrega una imagen", Toast.LENGTH_LONG).show();

        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        final MenuItem listUsersss= menu.findItem(R.id.drawListUser);
        final MenuItem addUser= menu.findItem(R.id.drawAddReport);
        //final MenuItem reportS= menu.findItem(R.id.drawReportSign);
        final MenuItem rCamera= menu.findItem(R.id.icCamera);
        //final MenuItem rQR= menu.findItem(R.id.addQR);

        db.collection("usuarios").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for(QueryDocumentSnapshot document : task.getResult()) {
                    Users user = document.toObject(Users.class);

                    //mUserList.clear();
                    //mUserList.add(user);
                    user.setId(document.getId());

                    Log.d("test", sessionEm, task.getException());
                    if(user.getEmail().equals(sessionEm) ){
                        currentrol=user.getRol();
                        Log.d("test", currentrol, task.getException());
                        if(currentrol.equals("Admin")){

                        }else{
                            listUsersss.setVisible(false);
                            if (currentrol.equals("Tecnico")) {


                            }else {
                                addUser.setVisible(false);
                                //reportS.setVisible(false);
                                rCamera.setVisible(false);
                                //rQR.setVisible(false);
                            }
                        }
                    }
                }


            }
        });
        //MenuItem list2=menu.findItem(R.id.draLogout);
        /*if(currentrol != "Admin") {
            listUsersss.setVisible(false);
        } *//*else*/


        //String temps=currentrol.toString();


        /*if(currentrol != "Admin") {
            listUsersss.setVisible(false);
            if (currentrol != "Tecnico") {
                addUser.setVisible(false);
                reportS.setVisible(false);
                rCamera.setVisible(false);
                rQR.setVisible(false);

            }else {

            }
        }else{
            addUser.setVisible(true);
            reportS.setVisible(true);
            rCamera.setVisible(true);
            rQR.setVisible(true);
        }*/

        //list2.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.drawListUser:
                startActivity(new Intent(getApplicationContext(),MainActivity.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawAddReport:
                startActivity(new Intent(getApplicationContext(),reporte.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawListReport:
                startActivity(new Intent(getApplicationContext(),listaReportes.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.draLogout:
                FirebaseAuth.getInstance().signOut();//logout
                startActivity(new Intent(getApplicationContext(),Login.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.icCamera:
                startActivity(new Intent(getApplicationContext(),activityQR.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            /*case R.id.drawReportSign:
                startActivity(new Intent(getApplicationContext(),reportSign.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.addQR:
                startActivity(new Intent(getApplicationContext(),createQr.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }
}
