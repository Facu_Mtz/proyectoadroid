package com.example.proyectoandroidv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.WriterException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class createQr extends AppCompatActivity {

    String TAG="GenerateQRCode";
    EditText edMessageQR;
    ImageView qrimg;
    Button btnCreateQR,btnSave;
    Bitmap bitmap;
    String inputvalue;
    QRGEncoder qrgEncoder;
    String path;
    String namefile;
    String namepath;
    String urlImage;
    ProgressDialog progressDialog ;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private static final String IMAGE_DIRECTORY = "/qrs";
    String currentrol;

    Uri FilePathUri;
    Uri File;
    StorageReference storageReference;
    DatabaseReference databaseReference;
    String sessionEm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_qr);

        qrimg=  findViewById(R.id.qrCodeImage);
        edMessageQR=findViewById(R.id.messageQR);
        btnCreateQR=findViewById(R.id.btnCreateQR);
        btnSave=findViewById(R.id.btnSaveQR);
        //currentrol=getIntent().getStringExtra("currentRol");

        sessionEm=getIntent().getStringExtra("currentEmail");


        storageReference = FirebaseStorage.getInstance().getReference("qr");
        databaseReference = FirebaseDatabase.getInstance().getReference("qr");
        /*btnbrowse = (Button)findViewById(R.id.btnbrowse);
        btnupload= (Button)findViewById(R.id.btnupload);
        txtdata = (EditText)findViewById(R.id.txtdata);
        imgview = (ImageView)findViewById(R.id.image_view);*/
        progressDialog = new ProgressDialog(createQr.this);// context name as per your project name

        btnCreateQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputvalue=edMessageQR.getText().toString().trim();
                if(inputvalue.length()>0){
                    WindowManager manager=(WindowManager)getSystemService(WINDOW_SERVICE);
                    Display display =manager.getDefaultDisplay();
                    Point point = new Point();
                    display.getSize(point);
                    int width=point.x;
                    int height=point.y;
                    int smallerdimension=width<height ? width:height;
                    smallerdimension=smallerdimension*3/4;
                    qrgEncoder=new QRGEncoder(inputvalue,null, QRGContents.Type.TEXT,smallerdimension);
                    try{
                        bitmap=qrgEncoder.encodeAsBitmap();
                        qrimg.setImageBitmap(bitmap);
                    }catch (WriterException e){
                        Log.v(TAG,e.toString());
                    }

                }else{
                    edMessageQR.setError("Required");
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BitmapDrawable drawable=(BitmapDrawable) qrimg.getDrawable();
                Bitmap bitmap=drawable.getBitmap();
                path = saveImage(bitmap);
                UploadImage();
            }
        });

    }
    public String saveImage(Bitmap myBitmap) {

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            //Bitmap bitmap=drawable.getBitmap();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File filepath= new File(
                    Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY /*iDyme folder*/);
            if (!filepath.exists()) {
                filepath.mkdirs();
                Log.d("test",filepath.toString());
            }
            try {
                File f = new File(filepath, Calendar.getInstance()
                        .getTimeInMillis() + ".jpg");
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
                MediaScannerConnection.scanFile(createQr.this,
                        new String[]{f.getPath()},
                        new String[]{"image/jpeg"}, null);
                namefile=f.getName();
                namepath=f.getAbsolutePath();

                FilePathUri= Uri.fromFile(new File(namepath));
                File=Uri.fromFile(new File(namefile));
                Log.i("Test",f.getPath());
                Log.i("Test",f.getName());
                Log.i("Test",f.getAbsolutePath());
                Log.i("Test",f.toString());
                Log.i("Test",f.getParent());


                fo.close();
                Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

                return f.getAbsolutePath();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return "";
    }

    public void UploadImage() {
        // FilePathUri=namepath.toString();
        if (FilePathUri != null) {

            progressDialog.setTitle("Image is Uploading...");
            progressDialog.show();
            urlImage=System.currentTimeMillis() + ".jpg";
            final StorageReference storageReference2 = storageReference.child(urlImage);
            storageReference2.putFile(FilePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            String TempImageName = namefile;
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Image Uploaded Successfully ", Toast.LENGTH_LONG).show();
                            @SuppressWarnings("VisibleForTests")
                            uploadinfo imageUploadInfo = new uploadinfo(TempImageName, taskSnapshot.getUploadSessionUri().toString());
                            String ImageUploadId = databaseReference.push().getKey();
                            databaseReference.child(ImageUploadId).setValue(imageUploadInfo);
                            Log.i("Test2",ImageUploadId);
                            Log.i("Test2",databaseReference.toString());
                            storageReference2.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Uri downloadUrl = uri;
                                    Log.i("Test2",downloadUrl.toString());
                                    //Do what you want with the url
                                    //String dates = edText.getText().toString().trim();
                                    //String service= edText2.getText().toString().trim();
                                    String urldef=downloadUrl.toString();
                                    //String cliente=edText3.getText().toString().trim();
                                    Map<String,Object> qrCol=new HashMap<>();

                                    qrCol.put("service","QR Image");
                                    qrCol.put("url",urldef);


                                    db.collection("qr").add(qrCol)
                                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                                @Override
                                                public void onSuccess(DocumentReference documentReference) {
                                                    Log.d("TAG","Se agegaron los datos con el id: "+documentReference.getId());

                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w("TAG","Error al agregar la imagen",e);
                                        }
                                    });
                                    Toast.makeText(createQr.this,"Se a agregado la imagen qr",Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

        }
        else {

            Toast.makeText(createQr.this, "Agrega una imagen", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        final MenuItem listUsersss= menu.findItem(R.id.drawListUser);
        final MenuItem addUser= menu.findItem(R.id.drawAddReport);
        //final MenuItem reportS= menu.findItem(R.id.drawReportSign);
        final MenuItem rCamera= menu.findItem(R.id.icCamera);
        //final MenuItem rQR= menu.findItem(R.id.addQR);

        db.collection("usuarios").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for(QueryDocumentSnapshot document : task.getResult()) {
                    Users user = document.toObject(Users.class);

                    //mUserList.clear();
                    //mUserList.add(user);
                    user.setId(document.getId());

                    Log.d("test", sessionEm, task.getException());
                    if(user.getEmail().equals(sessionEm) ){
                        currentrol=user.getRol();
                        Log.d("test", currentrol, task.getException());
                        if(currentrol.equals("Admin")){

                        }else{
                            listUsersss.setVisible(false);
                            if (currentrol.equals("Tecnico")) {


                            }else {
                                addUser.setVisible(false);
                               // reportS.setVisible(false);
                                rCamera.setVisible(false);
                                //rQR.setVisible(false);
                            }
                        }
                    }
                }


            }
        });

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.drawListUser:
                startActivity(new Intent(getApplicationContext(),MainActivity.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawAddReport:
                startActivity(new Intent(getApplicationContext(),reporte.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.drawListReport:
                startActivity(new Intent(getApplicationContext(),listaReportes.class).putExtra("currentEmail",sessionEm));
                return true;
            case R.id.draLogout:
                FirebaseAuth.getInstance().signOut();//logout
                startActivity(new Intent(getApplicationContext(),Login.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.icCamera:
                startActivity(new Intent(getApplicationContext(),activityQR.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            /*case R.id.drawReportSign:
                startActivity(new Intent(getApplicationContext(),reportSign.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;
            case R.id.addQR:
                startActivity(new Intent(getApplicationContext(),createQr.class).putExtra("currentEmail",sessionEm));
                finish();
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }
}
