package com.example.proyectoandroidv2;

import java.io.Serializable;

public class repotSignature implements Serializable {
    String service;
    String date;
    String url;
    String reportId;
    String client;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;

    public repotSignature(){

    }
    public repotSignature(String service, String date,String url,String client){
        this.date=date;
        this.service=service;

        this.url=url;
        this.client=client;
    }
    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String gereportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
