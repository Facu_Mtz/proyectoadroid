package com.example.proyectoandroidv2;

import java.io.Serializable;

public class Users implements Serializable {
    String nombre;
    String rol;
    String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;

    public Users(){

    }
    public Users(String nombre, String rol,String email){
        this.nombre=nombre;
        this.rol=rol;
        this.email=email;
    }

    public  String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre){
        this.nombre=nombre;
    }

    public String getRol(){
        return rol;
    }

    public void setRol(String rol){
        this.rol=rol;
    }
    public  String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email=email;
    }


}
