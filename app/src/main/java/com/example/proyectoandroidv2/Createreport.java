package com.example.proyectoandroidv2;

public class Createreport {

    String service;
    String date;
    String idEquip;
    String nameTec;
    String client;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdEquip() {
        return idEquip;
    }

    public void setIdEquip(String idEquip) {
        this.idEquip = idEquip;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getNameTec() {
        return nameTec;
    }

    public void setNameTec(String nameTec) {
        this.nameTec = nameTec;
    }
}
